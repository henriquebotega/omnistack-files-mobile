import React, { Component } from 'react'
import { Text, View, Image, TextInput, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import api from '../../services/api'

import logo from '../../assets/logo.png'

import styles from './styles'

class Main extends Component {

    state = {
        newBox: ''
    }

    async componentDidMount() {
        const box = await AsyncStorage.getItem("@omini:box")

        if (box) {
            this.props.navigation.navigate('Box')
        }
    }

    handleSignIn = async () => {
        const res = await api.post('boxes', {
            title: this.state.newBox
        })

        await AsyncStorage.setItem('@omini:box', res.data._id)

        this.props.navigation.navigate('Box')
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={logo} />

                <TextInput
                    style={styles.input}
                    placeholder="Crie um box"
                    placeholderTextColor="#999"
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={this.state.newBox}
                    onChangeText={t => this.setState({ newBox: t })}
                    underlineColorAndriod="transparent" />

                <TouchableOpacity style={styles.button} onPress={this.handleSignIn}>
                    <Text style={styles.buttonText}>Criar</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default Main