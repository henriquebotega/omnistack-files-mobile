import React, { Component } from 'react'
import { Text, View, FlatList, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import api from '../../services/api'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

import ImagePicker from 'react-native-image-picker'

import RNFS from 'react-native-fs'
import FileViewer from 'react-native-file-viewer'
import socket from 'socket.io-client'

import { distanceInWords } from 'date-fns'
import pt from 'date-fns/locale/pt'

class Box extends Component {

    state = {
        box: {}
    }

    async componentDidMount() {
        this.subscribeToNewFiles();

        const box = await AsyncStorage.getItem("@omini:box")
        const res = await api.get('boxes/' + box)

        this.setState({
            box: res.data
        })
    }

    subscribeToNewFiles = () => {
        const idbox = await AsyncStorage.getItem("@omini:box")

        // baseURL: 'https://oministack-files-backend.herokuapp.com'
        // baseURL: 'http://localhost:3333'

        const io = socket('http://localhost:3333')
        io.emit('connectRoom', idbox);

        io.on('file', data => {
            this.setState({
                box: {
                    ...this.state.box,
                    files: [data, ...this.state.box.files]
                }
            })
        })
    }

    openFile = async (file) => {
        try {

            const filePath = RNFS.DocumentDirectoryPath + '/' + file.title;

            await RNFS.downloadFile({
                fromUrl: file.url,
                toFile: filePath
            })

            await FileViewer.open(filePath)

        } catch (err) {
            console.log('Arquivo não suportado')
        }
    }

    renderItem = ({ item }) => (
        <TouchableOpacity style={styles.file} onPress={() => this.openFile(item)}>
            <View style={styles.fileIcon}>
                <Icon name="insert-drive-file" size={24} color="#a5cfff" />
                <Text style={styles.fileTitle}>{item.title}</Text>
            </View>

            <Text style={styles.fileDate}>há {distanceInWords(item.createdAt, new Date(), { locale: pt })}</Text>
        </TouchableOpacity>
    )

    handleUpload = () => {
        ImagePicker.launchImageLibrary({}, async upload => {
            if (upload.error) {
                console.log('Ocorreu um erro')
            } else if (upload.didCancel) {
                console.log('Cancelado pelo usuario')
            } else {
                const data = new FormData();

                const [prefix, suffix] = upload.fileName.split('.')
                const ext = suffix.toLowerCase() == 'heic' ? 'jpg' : suffix

                data.append('file', {
                    uri: upload.uri,
                    type: upload.type,
                    name: prefix + '.' + ext
                })

                api.post('boxes/' + this.state.box._id, data)
            }
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.boxTitle}>{this.state.box.title}</Text>

                <FlatList
                    data={this.state.box.files}
                    style={styles.list}
                    keyExtractor={f => f._id}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    renderItem={this.renderItem}
                />

                <TouchableOpacity style={styles.fab} onPress={this.handleUpload}>
                    <Icon name="cloud-upload" size={24} color="#fff" />
                </TouchableOpacity>
            </View>
        )
    }
}

export default Box